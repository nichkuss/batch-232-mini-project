$('#detpelamar').click(function() 
{
	pelamarmodal();
});

function pelamarmodal() 
{
	$.ajax({
		url : '/pelamardetail',
		type : 'get',
		dataType : 'html',
		success : function(result) 
		{
			$('#pelamarmodal').modal({backdrop: 'static', keyboard: false}) 
			$('#pelamarisi').html(result);
		}
	});
}

function refreshPage()
{
	$.ajax({
		url : '/',
		type : 'get',
		dataType : 'html',
		success : function(result) {
			closeModal();
			$('#fragment').html(result);
		}
	});
	return false;
}