function editOrganisasi(t) 
{
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) 
		{
			$('#organisasiList').html(result);
		}
	});
}

function deleteOrganisasi(t) 
{
	var alamat = t.getAttribute("href");
	$.ajax({
		url : alamat,
		type : 'get',
		dataType : 'html',
		success : function(result) 
		{
			$('#organisasiList').html(result);
		}
	});
}