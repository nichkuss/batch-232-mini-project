package com.mipro.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mipro.model.organisasiModel;
import com.mipro.repository.organisasiRepository;

@Service
@Transactional
public class organisasiService 
{
	@Autowired
	private organisasiRepository or;
	
	public List <organisasiModel> semuaOrganisasi()
	{
		return or.semuaOrganisasi();
	}
	
	public organisasiModel simpanOrganisasi(organisasiModel organisasiModel)
	{
		return or.save(organisasiModel);
	}
	
	public organisasiModel organisasiById(long id)
	{
		return or.organisasiById(id);
	}
	
	public void hapusOrganisasi(long id)
	{
		or.deleteFlag(id);
	}
}
