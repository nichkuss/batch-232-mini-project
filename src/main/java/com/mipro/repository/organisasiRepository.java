package com.mipro.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.mipro.model.organisasiModel;

public interface organisasiRepository extends JpaRepository <organisasiModel, Integer>
{
	@Query("select o from organisasiModel o where o.isDelete = false order by o.id")
	List <organisasiModel> semuaOrganisasi();
	
	@Query("select o from organisasiModel o where o.id=?1")
	organisasiModel organisasiById (long id);
	
	@Modifying
	@Query("update organisasiModel set isDelete = true where id=?1")
	long deleteFlag(long id);
}
